<?php
require_once("00_comprueba_datos.php");


//NAME LA BASE DE DATOS
$nombre_de_la_BD = "a14javmatfil_bd";

//CREAR TABLA TALLER
$taller = "$nombre_de_la_BD.taller";
$tabla = "CREATE TABLE IF NOT EXISTS $taller(
id int AUTO_INCREMENT,
nom varchar(20),
ubicacio char(100),
PRIMARY KEY(id)
)ENGINE=InnoDB";

if ($db_server->query($tabla) === FALSE) {
 	echo "Error creando la tabla: " . $db_server->error;
}

//CREAR TABLA VEHICULO
$vehiculo = "$nombre_de_la_BD.vehicle";
$tabla = "CREATE TABLE IF NOT EXISTS $vehiculo(
matricula char(9),
tipus varchar(10),
nom_complet varchar(60),
email varchar(30),
PRIMARY KEY(matricula)
)ENGINE=InnoDB";

if ($db_server->query($tabla) === FALSE) {
 	echo "Error creando la tabla: " . $db_server->error;
}

//CREAR TABLA CITA
$cita = "$nombre_de_la_BD.cita";
$tabla = "CREATE TABLE IF NOT EXISTS $cita(
matricula char(9),
id int,
data date,
hora varchar(10)
)ENGINE=InnoDB";

if ($db_server->query($tabla) === FALSE) {
 	echo "Error creando la tabla: " . $db_server->error;
}

$alter1 = "ALTER TABLE $cita ADD CONSTRAINT FOREIGN KEY (matricula) REFERENCES vehicle(matricula) ON DELETE CASCADE ON UPDATE CASCADE"; 
if ($db_server->query($alter1) === FALSE) {
 	echo "Error: " . $db_server->error;
}

$alter2 = "ALTER TABLE $cita ADD CONSTRAINT FOREIGN KEY (id) REFERENCES taller(id) ON DELETE CASCADE ON UPDATE CASCADE"; 
if ($db_server->query($alter2) === FALSE) {
 	echo "Error: " . $db_server->error;
}

$alter3 = "ALTER TABLE $cita ADD KEY (id,matricula,hora,data)"; 
if ($db_server->query($alter1) === FALSE) {
 	echo "Error: " . $db_server->error;
}


$valores3 = "INSERT INTO $taller(id,nom,ubicacio) VALUES('1','IAM','Avinguda d\'Esplugues 37');";

if (mysqli_multi_query($db_server, $valores3) === FALSE) {
    echo "Error: " . $valores2 . "<br>" . mysqli_error($db_server);
}



echo "Todo OK";
?>
