# README #

**Nom del projecte:**

  ITV   -  JAG   -  Grup3    
  
**Nom dels integrants del grup**

      Javier Maturana (Portaveu)
      
      Aitor Recio
      
      Gersson Veizaga
      
**Adreça del projecte al Bitbucket (recordeu deixar-lo públic)**

[Bitbucket](https://bitbucket.org/javiausias/itv "Bitbucket").      
      
**Adreça del full de seguiment (visible per tothom @iam.cat)**

[Seguiment](https://drive.google.com/open?id=1b5oSLH9oY2R27h0uzCr_sY7x22rv7h8xotiZlVLbVJU "Seguiment").
      
**Adreça del prototip (moqup)**

[Moqup](https://app.moqups.com/a14gerveiarc@iam.cat/uVCEYZMOlK/view "Moqup").

**Adreça del labs**
[Labs](http://labs.iam.cat/~a14javmatfil/)