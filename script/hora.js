$(document).ready(function(){
  
  var cuenta_li= function(){
    var cuantosLi = 0;
        $("ul > li").each(function(index) {
         cuantosLi = cuantosLi+1;
    });
    return cuantosLi;
  } 
  var h,m;
  var n_li=cuenta_li();
  var control;
  var hora_tabla='';
  var compruebaHora = function(){
    i=0;$("td").css("background-color","#4D4D4F");
            $(".ocupado").css("background-color","black");
            $(this).css("background-color","#F56200");
            $(this).css("color","black");
    $("li").each(function(){
                  text_li=$(this).text();
                  hora_li=text_li.substr(0,5);
                  ocu_li=parseInt(text_li.substring(8,9));
                  if(h<10 && m<10)
                    hora_tabla='0'+h+':'+'0'+m;
                  else if(h<10 && m>10)
                    hora_tabla='0'+h+':'+m;
                  else if(h>10 && m<10)
                    hora_tabla=h+':'+'0'+m;
                  else
                    hora_tabla=h+':'+m;
        (hora_li+" - "+ocu_li+" "+hora_tabla);
                  if(hora_li===hora_tabla && ocu_li==2){
                    control=true;
                    return control;
                  }else if(i==(n_li-1) || n_li===0){
                    control=false;
                    return control;
                  }
                i++;
                });
    if(control)
      return true;
    else 
      return false;
  }
  
  
  var datos={
    horario:{horas:8,minutos:0},
    code:''
  };
  
  //Modelo
  var model={
    generateTime:function(){
      datos.code+='<table border=1>';
         (datos.code);
      for(h=8;h<20;h++){
        datos.code+='<tr>';
        for(m=0;m<60;m+=30){
          if(compruebaHora())
            datos.code+='<td class="ocupado">';
          else
            datos.code+='<td>';
          if(h<10)
            datos.code+='0'+h;
          else
            datos.code+=h;
          datos.code+=':';
          if(m!==0)
            datos.code+=m;
          else{
            datos.code+=m;
            datos.code+=m;
          }
          datos.code+='</td>';
        }
      }
      datos.code+='</table>';
    }
  };
  
  //Vista
  var view={
    start: function(){
      controller.generateTime();
    },
    printTime:function(){
      $("#imprimeHora").html(datos.code);
      $("td").click(function(){
        if ($(this).hasClass("ocupado")){
        }else{
          $("td").css("background-color","white");
          $("td").css("color","black");
          $(".ocupado").css("background-color","#282C34");
          $(".ocupado").css("color","#606c76");
          $(this).css("background-color","orange");
          $(this).css("color","white");
          $("#hora").attr("value",$(this).text());

        }
      });
    }
  };
  
  //Controlador
  var controller={
    start: function(){
      view.start();
    },
    generateTime: function(){
      model.generateTime();
      view.printTime();
    }
  };
  
  controller.start();
  
});
