$("document").ready(function(){

  var cuenta_li= function(){
    var cuantosLi = 0;
        $("ul > li").each(function(index) {
         cuantosLi = cuantosLi+1;
    });
    return cuantosLi;
  }
  var n;
  var n_li=cuenta_li();
  var control;
  console.log(n_li);
  var i=0;
  var d= new Date();
  var dia=d.getDate();
  var mes=d.getMonth();
  var any=d.getFullYear();
  var datos={
    currDate: 
      {dia:dia,mes:mes,any:any},
    months: ["Gener","Febrer","Març","Abril","Maig","Juny","Juliol","Agost","Setembre","Octubre","Novembre","Decembre"],
    code:'',
    sendDate:''
  }
  var compruebaDia = function(){
    i=0;
    var nova_data=new Date(datos.currDate.any,datos.currDate.mes,n);
    var test_sun=nova_data.toString();
    test_sun=test_sun.substring(0,3);
    $("li").each(function(){
                  text_li=$(this).text();
                  dia_li=parseInt(text_li.substring(8,10));
                  ocu_li=parseInt(text_li.substring(13,15));
console.log(dia_li+" "+ocu_li+" "+i);
                  if(dia_li==n && ocu_li==48){
                    console.log("Entro porque estoy ocupado "+n);
                    control=true;
                    return control;
                  }else if(n<datos.currDate.dia){
                    console.log("Entro porque soy menor "+n);
                    control=true;
                    return control;
                  }else if(test_sun==='Sun'){
                    control=true;
                    return control;
                  }else if(i==(n_li-1)){
                    console.log("No sé ni porqué pongo algo aquí... "+n);
                    control=false;
                    return control;
                  }
                i++;
                });
    if(control)
      return true;
    else 
      return false;
  }
  //Modelo
  var model={
    changeMonth: function(n){
      datos.currDate.dia=1;
      if(datos.currDate.mes+n==12){
        datos.currDate.mes=0;
        datos.currDate.any+=1;
      }else if(datos.currDate.mes+n==-1){
        datos.currDate.mes=11;
        datos.currDate.any-=1;
      }else{
        datos.currDate.mes+=n;
      }
    },
    generateCalendar: function(){
      var dia_li,ocu_li,text_li;
      var d2=new Date(datos.currDate.any,datos.currDate.mes,1);
      var str=d2.toString();
      str=str.substring(0,3);
      if(str=="Mon")
        n=1;
      else if(str=="Tue")
        n=0;
      else if(str=="Wed")
        n=-1;
      else if(str=="Thu")
        n=-2;
      else if(str=="Fri")
        n=-3;
      else if(str=="Sat")
        n=-4;
      else
        n=-5;
      datos.code+='<table border=1>';datos.code+='<tr><th>L</th><th>M</th><th>X</th><th>J</th><th>V</th><th>S</th><th>D</th></tr>';
       
      for(sem=0;sem<6;sem++){
          if(((datos.currDate.mes===0 || datos.currDate.mes==2 || datos.currDate.mes==4 || datos.currDate.mes==6 || datos.currDate.mes==7 || datos.currDate.mes==9 || datos.currDate.mes==11) && n<=31)|| ((datos.currDate.mes==3 || datos.currDate.mes==5 || datos.currDate.mes==8 || datos.currDate.mes==10) && n<31) || ((datos.currDate.mes==1) && n<29)){
              datos.code+='<tr>';
            for(dia=0;dia<7;dia++){
              if(datos.currDate.mes===0 || datos.currDate.mes==2 || datos.currDate.mes==4 || datos.currDate.mes==6 || datos.currDate.mes==7 || datos.currDate.mes==9 || datos.currDate.mes==11){
                if(compruebaDia())
                  datos.code+='<td class="ocupado">';
                else
                  datos.code+='<td>';
                if(n>=1 && n<=31)
                  if(n<10)
                    datos.code+='0'+n;
                  else
                    datos.code+=n;
                n++;
              }else if(datos.currDate.mes==1){
                datos.code+='<td>';
                if(n>=1 && n<=28){
                  if(n<10)
                    datos.code+='0'+n;
                  else
                    datos.code+=n;
                }else if(n==29 && (datos.currDate.any%4===0 && (datos.currDate.any%100!==0 || datos.currDate.any%400===0))){
                  datos.code+=n;
                }
                n++;
              }else if(datos.currDate.mes==3 || datos.currDate.mes==5 || datos.currDate.mes==8 || datos.currDate.mes==10){
                datos.code+='<td>';
                if(n>=1 && n<=30){
                  if(n<10)
                    datos.code+='0'+n;
                  else
                    datos.code+=n;
                }
                n++;
              }
              datos.code+='</td>';
            }
            datos.code+='</tr>';
          }
        }
    }
  };
   //Vista
  var view={
      start: function(){
        controller.generateCalendar();
        $("#next").click(function(){
          controller.changeMonthNext();
        });
        $("#prev").click(function(){
          controller.changeMonthPrev();
        });
        
      },
      printCalendar: function(){
        
        $("#imprimeFecha").hide();
        $("#Mes").text(datos.months[datos.currDate.mes]+"/"+datos.currDate.any);
        $("#calendari").html(datos.code);
        $("td").click(function(){
          if ($(this).hasClass("ocupado") || !$.isNumeric(parseInt($(this).text()))){
          }
          else{
            $("td").css("background-color","#4D4D4F");
            $("td").css("color","white");
            $(".ocupado").css("color","white");
            $(".ocupado").css("background-color","black");
            $(this).css("background-color","#F56200");
            $(this).css("color","black");
          
          datos.sendDate=datos.currDate.any+"-"+(datos.currDate.mes+1)+"-"+(parseInt($(this).text()));
          localStorage.setItem("fullDate",datos.sendDate);
          $("#imprimeFecha2").html(datos.sendDate);
          $("#imprimeFecha").attr("value",datos.sendDate);
          }
        });
      }
  };

  //Controlador
  var controller={
    start: function(){
      view.start();
    },
    generateCalendar: function(){
      model.generateCalendar();
      view.printCalendar();
      datos.code='';
    },
    changeMonthPrev: function(){
      model.changeMonth(-1);
      model.generateCalendar();
      view.printCalendar();
      datos.code='';
    },
    changeMonthNext: function(){
      model.changeMonth(1);
      model.generateCalendar();
      view.printCalendar();
      datos.code='';
    }
  };
  
   
  controller.start();
});
