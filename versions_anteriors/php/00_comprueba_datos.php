<?php
//Logueamos
require_once '000_login.php';
//NOS CONECTAMOS AL SERVIDOR
$db_server = new mysqli($servername, $username, $password);
//Comprobamos la conexion
if ($db_server->connect_error) {
   die("Connection failed: " . $db_server->connect_error);
}

//CREAMOS LA BASE DE DATOS
$nombre_de_la_BD = "formulari";
$base_de_datos = "CREATE DATABASE IF NOT EXISTS $nombre_de_la_BD";
//COMPROBAMOS SI SE HA CREADO CORRECTAMENTE
if ($db_server->query($base_de_datos) === FALSE) {
	echo "Error creando la BD " . $db_server->error;
}
####################################################################
//CREAR TABLA TALLER
$taller = "$nombre_de_la_BD.taller";
$tabla = "CREATE TABLE IF NOT EXISTS $taller(
id int AUTO_INCREMENT,
nom varchar(20),
ubicacio char(100),
PRIMARY KEY(id)
)ENGINE=InnoDB";

if ($db_server->query($tabla) === FALSE) {
 	echo "Error creando la tabla: " . $db_server->error;
}

//CREAR TABLA VEHICULO
$vehiculo = "$nombre_de_la_BD.vehicle";
$tabla = "CREATE TABLE IF NOT EXISTS $vehiculo(
matricula char(9),
tipus varchar(10),
PRIMARY KEY(matricula)
)ENGINE=InnoDB";

if ($db_server->query($tabla) === FALSE) {
 	echo "Error creando la tabla: " . $db_server->error;
}

//CREAR TABLA CITA
$cita = "$nombre_de_la_BD.cita";
$tabla = "CREATE TABLE IF NOT EXISTS $cita(
matricula char(9),
id int,
data date,
hora varchar(10),
PRIMARY	KEY (matricula,id,data,hora),
FOREIGN KEY (matricula) REFERENCES vehicle(matricula),
FOREIGN KEY (id) REFERENCES taller(id)
)ENGINE=InnoDB";

if ($db_server->query($tabla) === FALSE) {
 	echo "Error creando la tabla: " . $db_server->error;
}

?>
