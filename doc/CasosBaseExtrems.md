**Que passa si…**

**El DNI és incorrecte?**
No hi ha DNI.

**El client posa camps buits?**
Tenim una comprovació de camps i també visual.

**L'usuari vol saber quan ha de passar la ITV?**
Tenim registre d'historial.

**Tenim moltes matricules?**
En dos anys s'esborra la base de dades.

**Algú posa una matricula ja registrada?**
Llavors li mostrem el llistat de cites.

**Vols saber si la data a vençut?**
Vol dir que l'usuari ha passat la ITV.

**Un client et demana quan li toca la ITV?**
Tenim el registre d'historial.

**No passa la ITV? S'ha de tornar a registrar o li donem data?**
Tots passen la ITV.

**Tenim una base de dades molt pesada?**
Cada dos anys esborrem.

**Es pot tenir 2 dates de la mateixa matricula?**
Dues dates iguals no. Dues dates diferents si.

**El client passa la ITV al centre IAM i alhora al centre JBosca?**
Es comprova els valors introduïts prèviament.

**En els camps ens poden indexar?**
Tenim valors que no permeten l'ús.
